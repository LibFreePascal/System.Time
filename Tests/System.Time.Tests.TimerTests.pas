unit System.Time.Tests.TimerTests;
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testutils, testregistry;

type
  TTimerTests = class(TTestCase)
  protected
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestHookUp;
  end;

implementation

procedure TTimerTests.TestHookUp;
begin
  Fail('Not implemented.');
end;

procedure TTimerTests.SetUp;
begin
end;

procedure TTimerTests.TearDown;
begin
end;

initialization
  RegisterTest(TTimerTests);
end.

